# Home

## Hello and Welcome!

# About me

<table><tr><td width=300><img src=images/wil-alt-rhein.jpg width=300></td><td><ol style="font-size: 0.85em;"><li>BSc Phys [McGill]</li><li>PhD Zool [UBC]</li><li>Postdoc BiomedSci [Wollongong]</li><li>Postdoc Chem [Wollongong]</li><li>Lecturer (Asst Prof) MechEng [Bath]</li><li>Adj Assoc Prof Geog [Victoria]</li><li>Professor Bionics [RhineWaal]</li></ol></td></tr></table>


I'm a Canadian sea captain and PhD biologist with a physics degree who teaches biomimetics and engineering design in a landlocked little town in Germany. I've studied whales, jellyfish, sea turtles and everything they eat. I'm a sea kayaking instructor and was once the "mayor" of the biggest summertime settlement on the north shore of Queen Charlotte Strait, British Columbia. I've seen flying fish race ahead of my boat in Baja, Mexico and avoided a brush with a stone fish diving in the Red Sea. I teach physics, biology, engineering and science communication, oftentimes all in one lecture. My first real job was designing women's underwear, my first scientific publication was a poem, and along the way somewhere I was briefly a professor of geography, but despite graduating 11 PhDs, really I'm best known for running a submarine race. I've been a CEO, a dean and a director of studies, landed a million euros in research grants, published 28 papers and a patent. I've burned out twice and had a stroke, but they still fear me on the football field. I was an Eagle Scout, a refugee, and a short order cook. Now I'm just over fifty and enjoying the living in life.

# A CV in Projects

This site highlights some of the elements of my 30-year career in R&D.
