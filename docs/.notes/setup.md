# 1. Setup Notes

## Template

Material with mkdocs

## Location

local: Documents/gitlab
remote: gitlab.com/megillw
web: gitlab.io/megillw/MyAwesomeProject

## git

### Set up the keys

to generate a key, use:

ssh-keygen -t rsa -b 2048 -C "wmm@hsrw.eu"

to see the key use

cat ~/.ssh/id_rsa.pub

That gets used somehow with the settings on the gitlab.com website.

### Establishing the connection

to establish the connection, clone the repository:

git clone git@gitlab.com:megillw/myAwesomeProject.git

or better:

git clone https://gitlab.com/megillw/myAwesomeProject.git

then pull the current state of the repository.

cd myAwesomeProject
git pull

to add files, use

git add .
git commit -m "coments"
git push

### Making it into a website

add the gitlab-ci.yml file to the base of the project

